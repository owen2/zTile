# zTile

This Gnome shell extension is based on the [gTile extension](https://github.com/vibou/vibou.gTile). This allows the extension to be used with the [Hide Top Bar extension](https://extensions.gnome.org/extension/545/hide-top-bar/).

This has not been tested with multiple monitors but it should work fine.
